webpackJsonp([2],{

/***/ 0:
/*!*******************************!*\
  !*** ./src/js/keypad/main.js ***!
  \*******************************/
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	var _socketIo = __webpack_require__(/*! ../common/socket-io */ 247);
	
	var _socketIo2 = _interopRequireDefault(_socketIo);
	
	var _offline = __webpack_require__(/*! ./views/offline */ 407);
	
	var _offline2 = _interopRequireDefault(_offline);
	
	var _register = __webpack_require__(/*! ./views/register */ 408);
	
	var _register2 = _interopRequireDefault(_register);
	
	var _slider = __webpack_require__(/*! ./views/slider */ 409);
	
	var _slider2 = _interopRequireDefault(_slider);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var offlineView = new _offline2.default();
	var registerView = new _register2.default({ onRegister: onRegister });
	var sliderView = new _slider2.default({ onChange: onChange });
	var node = document.getElementById('app');
	
	var forceRefresh = false;
	var currentView = void 0;
	
	var dateInit = Date.now();
	function onRegister(id) {
		_socketIo2.default.emit('register-device', id, function (err) {
			if (err) return console.log(err);
			window.localStorage.setItem('lastUsedId', id);
			return render(true, id);
		});
	}
	
	function onChange(value) {
		if (dateInit < Date.now(-300)) {
			dateInit = Date.now();
			_socketIo2.default.emit('value-change', value);
		}
	}
	
	_socketIo2.default.on('connect', function () {
		if (forceRefresh) {
			window.location.reload();
		}
	
		render(true);
	});
	
	_socketIo2.default.on('disconnect', function () {
		forceRefresh = true;
		render(false);
	});
	
	_socketIo2.default.on('device-destroyed', function () {
		render(true);
	});
	
	function render(online, id, value) {
		var newView = void 0;
	
		// if(!online) newView = offlineView;
		if (!online) newView = registerView;else if (id) newView = sliderView;else newView = registerView;
	
		if (currentView) currentView.remove();
	
		newView.appendTo(node);
	
		currentView = newView;
	}

/***/ }),

/***/ 407:
/*!****************************************!*\
  !*** ./src/js/keypad/views/offline.js ***!
  \****************************************/
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports.default = Offline;
	function Offline() {
		var el = document.createElement('div');
		el.innerHTML = '<div class="offline-view">\n\t\t\t\t\t\t<div class="offline-wireframe">\n\t\t\t\t\t\t\t<p>Conex\xE3o interrompida!</p>\n\t\t\t\t\t\t<div id="score"><img src="/assets/layerbranco.png"/></div>\n\t\t\t\t\t\t</div>\t\n\t\t\t\t\t</div>';
	
		this.appendTo = function (node) {
			document.body.ontouchmove = function (e) {
				e.preventDefault();
			};
			node.appendChild(el);
		};
	
		this.remove = function () {
			document.body.ontouchmove = null;
			el.parentNode.removeChild(el);
		};
	}

/***/ }),

/***/ 408:
/*!*****************************************!*\
  !*** ./src/js/keypad/views/register.js ***!
  \*****************************************/
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports.default = Register;
	var lastUsedId = window.localStorage.getItem('lastUsedId') || '';
	
	function Register(settings) {
		var el = void 0,
		    input = void 0,
		    button = void 0;
	
		el = document.createElement('div');
		el.innerHTML = '<div class="register-view">\n\t\t\t\t\t\t<img src="/assets/Logo_interativa.png">\n\t\t\t\t\t\t<div id="register-message">Insira o n\xFAmero do seu dispositivo</div>\n\t\t\t\t\t\t<input id="register-input" type="number" value="' + lastUsedId + '" />\n\t\t\t\t\t\t<span id="register-button" class="btn btn-pink">Clique para conectar!</span>\n\t\t\t\t\t</div>';
	
		button = el.querySelector('#register-button');
		input = el.querySelector('input');
	
		function addListeners() {
			button.onclick = function (event) {
				event.preventDefault();
				settings.onRegister(input.value);
			};
	
			document.body.ontouchmove = function (e) {
				e.preventDefault();
			};
		}
	
		function removeListeners() {
			button.onclick = null;
			document.body.ontouchmove = null;
		}
	
		this.appendTo = function (node) {
			node.appendChild(el);
			addListeners();
		};
	
		this.remove = function () {
			removeListeners();
			el.parentNode.removeChild(el);
		};
	}

/***/ }),

/***/ 409:
/*!***************************************!*\
  !*** ./src/js/keypad/views/slider.js ***!
  \***************************************/
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports.default = Slider;
	
	var _socketIo = __webpack_require__(/*! ../../common/socket-io */ 247);
	
	var _socketIo2 = _interopRequireDefault(_socketIo);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	console.log(window.innerHeight);
	function Slider(settings) {
		var el = void 0,
		    interval = void 0,
		    lastvalue = void 0,
		    value = void 0,
		    porcentagemNova = void 0,
		    gradientvalue = 56.5;
		var slider, output, volume, gradient;
		// let lefty = false;
	
	
		el = document.createElement('div');
		el.innerHTML = '\n\t<div class="fixed">\n\t<div id="gradient"></div>\n\t\t\t\t\t<div class="slider-view">\n\t\t\t\t\t\t<div id="principal">\n\t\t\t\t\t\t\t<div class="output">50</div>\n\t\t\t\t\t\t\t<div id="volume">\n\t\t\t\t\t\t\t<div id="divisoes"></div>\n\t\t\t\t\t\t\t\t<div id="num">100</div>\n\t\t\t\t\t\t\t\t<div id="num2">50</div>\t\n\t\t\t\t\t\t\t\t<div id="num3">0</div>\n\t\t\t\t\t\t\t<div class="slider"></div>\t\t\t\t\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>';
		//slider é um seletor global criado por ele ser usado muitas vezes assim como os demais abaixo, slider é equivalente ao 'botão' da view
		slider = el.querySelector('.slider');
		//gradient é responsável ao gradient rosa da view
		gradient = el.querySelector('#gradient');
		//volume é responsável a barra/tamanho da barra da view
		volume = el.querySelector('#volume');
		//output é responsável pelo score da view
		output = el.querySelector('.output');
	
		//TODO não sabemos
		function forceDefaultvalue(newvalue) {
			value = newvalue;
			return onChange();
		}
	
		//TODO não sabemos
		function emitChange() {
			console.log(value);
			if (value != lastvalue) {
				lastvalue = value;
				settings.onChange(value);
			}
		}
	
		//altera o valor do slider e do gradiente
		//Antonio: Função que é chamada quando se recebe uma mudança de valor 
		// ou seja, quando o meu valor é modificado, seja por clique, ou por breakpoint, essa função será chamada
		function onChange() {
			//altura máxima do gradient, 22 é equivalente a 100
			//Antonio:quando o valor muda, é realizado um calculo para verificar qual a posição top do meu gradiente, baseando-se nesse novo valor.
			//ou seja, se o meu novo valor for (value=37) irá se fazer as contas para verificar como deve se comportar o top do gradiente.	
	
			//########GRADIENTE############
			// var maxGradient = 22;
			//altura mínima do gradient, 100 é equivalente a 0
			// var minGradient = 100;
			//inverte o valor de 100 a 0 passa a ser 0 a 100 
			var invertedValue = 100 - value;
			//0.69 é obtido quando 34,5 é dividido por 50 (34,5 é 56,5 - 22), no geral ela é definida para o gradient ficar em 50% 
			var newGradientValue = invertedValue * 0.69 + 22;
	
			//altera o top do gradient
			gradient.style.top = newGradientValue + '%';
			//inicio altura máxima do botão
			//########GRADIENTE############
	
	
			//##IDEM AO GRADIENTE POREM EM RELAÇÃO AO SLIDER##
			// var sliderInit = -2;
			var sliderValue = invertedValue * 0.95 - 5;
			console.log(sliderValue);
			console.log('sliderValue');
			slider.style.top = sliderValue + '%';
	
			//##IDEM AO GRADIENTE POREM EM RELAÇÃO AO SLIDER##
	
	
			//adiciona o valor ao score
			//Antonio: será adicionado ao HTML, onde output é
			output.innerHTML = value;
	
			//força valor do score
			//Antonio: em algum momento recebemos uma variavel porcentagem nova que não faço ideia do que seja mas quando o usuário realiza touch na tela, e vai alem dos limites
			//criou-se um if para que o comportamento do botão e gradiente não extrapolassem o limite
			//Anna: Não precisa mais desses ifs abaixo, anteriormente fazia o que o comentário acima descrevia, porém trava o botão nas extremidades quando dado o breakpoint
			// if(porcentagemNova >= 100){
			// 	slider.style.top = '-4%';
			// }
			//   if(porcentagemNova <= -2){
			//    	slider.style.top = '93%';
			//   }
		}
	
		//valor de onde você clica
		//Antonio: função que será chamada ao clicar em um elemento, não é a função nativa do html foi uma função criada 
		//event é o parâmetro que é passado informando, propriedades de onde foi realizado o clique (y/x)
		function onTouch(event) {
	
			if (!el.parentNode) return;
	
			//########################################################
			//########################################################		
			//CALCULO PARA IDENTIFICAR O VALOR DE ONDE FOI CLICADO
			//########################################################
			//########################################################		
			//altura máxima do slider(botão) 
			var pontoInicial = window.innerHeight * 240 / 1024;
			//240
			//927
			//altura mínima
			var pontoFinal = window.innerHeight * 927 / 1024;
			var variacao = pontoFinal - pontoInicial;
			//event touches = lugar onde cliquei de acordo com a posição y
			var valor = event.touches[0].clientY - pontoInicial;
	
			event.preventDefault();
	
			var porcentagem = Math.round(100 - valor * 100 / variacao);
			porcentagemNova = porcentagem;
			var porcentagemInvertida = Math.round(valor * 100 / variacao);
	
			console.log("porcentagem value");
			console.log(porcentagem);
	
			if (porcentagem >= 100) {
				porcentagem = 100;
			}
	
			if (porcentagem <= 0) {
				porcentagem = 0;
			}
			var windowHeight = window.innerHeight - 272.72;
			var y = event.touches[0].clientY - 272.72;
			value = porcentagem;
			// if (value >= 100){
			// 	value = 100;
			// }
			// value = Math.round(100 * y / windowHeight);
			// value = value < 0 ? 0 : value;
			// value = value > 100 ? 100 : value;
	
	
			return Secondvalue(event);
		}
	
		//o valor de onde vc clica no gradiente
		function Secondvalue(event) {
			//altura máxima do gradient
	
			var initial = window.innerHeight * 233 / 974;
			//altura mínima
			var ending = window.innerHeight * 931 / 974;
			var varia = ending - initial;
			var valuees = event.touches[0].clientY - initial;
	
			event.preventDefault();
	
			var percent = Math.round(100 - valuees * 100 / varia);
			var percentin = Math.round(valuees * 100 / varia);
	
			console.log('porcentagem gradient');
			console.log(percent);
	
			var windowSecondHeight = window.innerHeight - 20;
			var y = event.touches[0].clientY - 10;
	
			gradientvalue = Math.round(100 * y / windowSecondHeight);
			gradientvalue = gradientvalue < 0 ? 0 : gradientvalue;
			gradientvalue = gradientvalue > 100 ? 100 : gradientvalue;
	
			console.log(event.touches[0]);
	
			return onChange();
		}
	
		//TODO
		//listeners escutam
	
		function addListeners() {
			interval = setInterval(emitChange, 1000);
	
			//TODO
			_socketIo2.default.on('force-default-value', forceDefaultvalue);
	
			if ('ontouchstart' in window || navigator.MaxTouchPoints > 0 || navigator.msMaxTouchPoints > 0) {
				volume.ontouchstart = onTouch;
				volume.ontouchmove = onTouch;
			} else {
	
				var isDragging = false;
	
				volume.onmousedown = function (event) {
					isDragging = true;
					console.log("isdragging");
					return onTouch({
						preventDefault: function preventDefault() {},
						touches: [{ clientY: event.clientY }]
					});
				};
	
				volume.onmouseup = function (event) {
	
					isDragging = false;
				};
	
				volume.onmousemove = function (event) {
					if (!isDragging) return;
					return onTouch({
						preventDefault: function preventDefault() {},
						touches: [{ clientY: event.clientY }]
					});
				};
			}
		}
	
		//TODO
		function removeListeners() {
			clearInterval(interval);
			_socketIo2.default.off('force-default-value', forceDefaultvalue);
			volume.ontouchstart = null;
			volume.ontouchmove = null;
			volume.onmousedown = null;
			volume.onmouseup = null;
			volume.onmousemove = null;
		}
	
		//TODo
		//addListeners
		this.appendTo = function (node) {
			addListeners();
			node.appendChild(el);
			onChange();
		};
	
		//TODO
		this.remove = function () {
			removeListeners();
			el.parentNode.removeChild(el);
		};
	}

/***/ })

});
//# sourceMappingURL=keypad.js.map