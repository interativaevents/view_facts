var _ = require('underscore');
var moment = require('moment');
var async = require('async');
var loopback = require('loopback');
var remoteMethod = require('../system/helpers').remoteMethod;
var app = require('../server');

function define(Activity)
{
	var io = require('../server').io;

	var Device = loopback.getModel('Device');
	var Record = loopback.getModel('Record');
	var Breakpoint = loopback.getModel('Breakpoint');

	var breakpoint = false;
	var running = false;
	var interval = null;
	var secondCounter = 0;
	var timeInit = 0;
	var eternalTimeInit = new Date().getTime()
	function refresh()
	{
		clearInterval(interval);
		io.emit('activity-stopped');

		breakpoint = false;
		running = false;
		interval = null;
		secondCounter = 0;

		async.series(
		[
			function(next) { Record.findOne({ order: 'second DESC', limit: 1 }, next); },
			function(next) { Breakpoint.findOne({ order: 'second DESC', limit: 1 }, next); }
		], function(err, results)
		{
			if(err) return;

			var lastRecordSecond = results[0] ? results[0].second : -1;
			var lastBreakpointSecond = results[1] ? results[1].second : -1;

			secondCounter = Math.max(lastRecordSecond, lastBreakpointSecond);

			if(secondCounter < 1) secondCounter = 0;
			else secondCounter++;

			console.log('secondCounter', secondCounter);
		});
	}

	refresh();
	app.on('database-loaded', refresh);

	function onInterval()
	{			
		var timeNow = new Date().getTime()
		var deltaSecond = timeNow-timeInit;	
		
		if(!running||deltaSecond<1000){
		 return;		
		}else{		
			var second = secondCounter++;			
			timeInit = timeNow-(deltaSecond-1000);
			console.log(deltaSecond)					
			console.log(timeNow-eternalTimeInit)
			async.waterfall(
			[
				function(next) { Device.find(null, next); },
				function(devices, next)
				{
					var records = [];

					_.each(devices, function(device)
					{
						if(device.connected && device.enabled)
						{
							records.push({
								deviceId: device.id,
								second: second,
								value: device.currentValue
							});
						}
					});
					console.log('time');
					console.log(new Date().getTime() - timeNow);
					io.emit('activity-new-values',
					{
						second: second,
						breakpoint: breakpoint,
						values: records
					});

					return async.parallel(
					[
						function(next) {							
							Record.create(records, next); },
						function(next)
						{
							console.log(records);
							if(breakpoint)
							{
								breakpoint = false;
								return Breakpoint.create({ second: second }, next);
							}
							else return next();
						}
					], next);
				}
			]
			, function(err)
			{
				if(err) console.log(err);
			});
		}
	}

	Activity.getDuration = function()
	{
		return secondCounter;
	};

	remoteMethod(Activity,
	{
		name: 'running',
		http: { verb: 'get' },
		returns: { type: 'string', root: true },
		definition: function(finalCallback)
		{
			return finalCallback(null, running);
		}
	});

	remoteMethod(Activity,
	{
		name: 'start',
		http: { verb: 'put' },
		returns: { type: 'boolean', root: true },
		definition: function(finalCallback)
		{
			if(!running)
			{
				running = true;
				
				Device.resetValue();
				io.emit('activity-started');
				timeInit = new Date().getTime()
				interval = setInterval(onInterval, 500);
			}

			return finalCallback(null, true);
		}
	});

	remoteMethod(Activity,
	{
		name: 'stop',
		http: { verb: 'put' },
		returns: { type: 'boolean', root: true },
		definition: function(finalCallback)
		{
			if(running)
			{
				running = false;
				clearInterval(interval);
				io.emit('activity-stopped');
			}

			return finalCallback(null, true);
		}
	});

	remoteMethod(Activity,
	{
		name: 'breakpoint',
		http: { verb: 'put' },
		returns: { type: 'boolean', root: true },
		definition: function(finalCallback)
		{
			if(running)
			{
				breakpoint = true;
			}
			
			return finalCallback(null, true);
		}
	});

	remoteMethod(Activity,
	{
		name: 'clear',
		http: { verb: 'put' },
		returns: { type: 'boolean', root: true },
		definition: function(finalCallback)
		{
			secondCounter = 0;

			return async.parallel(
			[
				function(next){ Record.destroyAll(null, next); },
				function(next){ Breakpoint.destroyAll(null, next); }
			], function(err)
			{
				if(!err) io.emit('activity-cleared');

				return finalCallback(err, !err);
			});
		}
	});

	remoteMethod(Activity,
	{
		name: 'values',
		http: { verb: 'get' },
		returns: { type: 'boolean', root: true },
		definition: function(finalCallback)
		{
			return async.parallel(
			[
				function(next){ Breakpoint.find(null, next); },
				function(next){ Record.find(null, next); }
			], function(err, results)
			{
				if(err) return finalCallback(err);

				var result = {};
				var breakpoints = _.indexBy(results[0], 'second');
				var records = _.groupBy(results[1], 'second');

				for(var second = 0; second < secondCounter; second++)
				{
					result[second] =
					{
						second: second,
						breakpoint: !!breakpoints[second],
						values: []
					}

					if(records[second])
					{
						result[second].values = records[second];
					}
				}

				return finalCallback(null, _.toArray(result));
			});
		}
	});
}

module.exports = function(Activity)
{
	Activity.on('attached', function()
	{
		app.on('started', function()
		{
			define(Activity);
		});
	});
};