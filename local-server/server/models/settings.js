var _ = require('underscore');
var disableAllMethods = require('../system/helpers').disableAllMethods;
var remoteMethod = require('../system/helpers').remoteMethod;
var app = require('../server');

var currentSettings;

function define(Settings)
{
	disableAllMethods(Settings);

	Settings.create(
	{
		id: 1,
		numberOfDevices: 10,
		defaultDeviceValue: 50
	});

	function refresh()
	{
		Settings.findById(1, function(err, result)
		{
			currentSettings = result;
			Settings.emit('change', currentSettings);
		});
	}

	app.on('database-loaded', refresh);

	setTimeout(refresh, 1);

	Settings.on('change', function(result)
	{
		// console.log('Settings changed', JSON.stringify(result));
	});

	remoteMethod(Settings,
	{
		name: 'get',
		http: { verb: 'get' },
		returns: { type: 'boolean', root: true },
		definition: function(finalCallback)
		{
			return finalCallback(null, currentSettings);
		}
	});

	remoteMethod(Settings,
	{
		name: 'set',
		http: { verb: 'post' },
		accepts: { arg: 'settings', type: 'object', root: true },
		returns: { type: 'boolean', root: true },
		definition: function(settings, finalCallback)
		{
			_.extend(settings, { id: 1 });

			return Settings.upsert(settings, function(err, result)
			{
				if(err) return finalCallback(err);

				currentSettings = result;
				Settings.emit('change', currentSettings);

				return finalCallback(null, currentSettings);
			});
		}
	});
}

module.exports = function(Settings) { Settings.on('attached', function() { define(Settings); }); };