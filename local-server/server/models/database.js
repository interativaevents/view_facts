var loopback = require('loopback');
var remoteMethod = require('../system/helpers').remoteMethod;
var app = require('../server');
var fs = require('fs');
var async = require('async');

var NOTHING_TO_DO = 'NOTHING_TO_DO';

function define(Database)
{
	var Device = loopback.getModel('Device');
	var memoryConnector = app.dataSources.memory.connector;
	console.log("Database Connector",memoryConnector.settings)
	remoteMethod(Database,
	{
		name: 'currentFile',
		http: { verb: 'get' },
		returns: { type: 'string', root: true },
		definition: function(callback)
		{
			return callback(null, memoryConnector.settings.file);
		}
	});

	remoteMethod(Database,
	{
		name: 'new',
		http: { verb: 'get' },
		returns: { type: 'boolean', root: true },
		definition: function(callback)
		{
			return async.waterfall(
			[
				function(next)
				{
					app.emit('show-save-dialog',
					{
						filters: [{ name: 'JSON File', extensions: ['json'] }]
					}, function(path) { next(null, path); });
				},
				function(path, next)
				{
					if(path)
					{
						memoryConnector.isTransaction = true;
						memoryConnector.settings.file = path;
						return memoryConnector.saveToFile(null, next);
					}

					return next(null, NOTHING_TO_DO);
				},
				function(nothing, next)
				{
					
				}
			], function(err, result)
			{
				if(err) return callback(err);
				if(result == NOTHING_TO_DO) return callback(null, false);
				return callback(err, true);
			});
		}
	});

	remoteMethod(Database,
	{
		name: 'save',
		http: { verb: 'get' },
		returns: { type: 'boolean', root: true },
		definition: function(callback)
		{
			return async.waterfall(
			[
				function(next)
				{
					app.emit('show-save-dialog',
					{
						filters: [{ name: 'JSON File', extensions: ['json'] }]
					}, function(path) { next(null, path); });
				},
				function(path, next)
				{
					if(path)
					{
						memoryConnector.isTransaction = true;
						memoryConnector.settings.file = path;
						return memoryConnector.saveToFile(null, next);
					}

					return next(null, NOTHING_TO_DO);
				}
			], function(err, result)
			{
				if(err) return callback(err);
				if(result == NOTHING_TO_DO) return callback(null, false);
				return callback(err, true);
			});
		}
	});

	remoteMethod(Database,
	{
		name: 'load',
		http: { verb: 'get' },
		returns: { type: 'boolean', root: true },
		definition: function(callback)
		{
			return async.waterfall(
			[
				function(next)
				{
					app.emit('show-open-dialog',
					{
						filters: [{ name: 'JSON File', extensions: ['json'] }]
					}, function(paths) { next(null, paths); });
				},
				function(paths, next)
				{
					if(paths && paths[0])
					{
						memoryConnector.isTransaction = true;
						memoryConnector.settings.file = paths[0];
						return memoryConnector.loadFromFile(next);
					}

					return next(null, NOTHING_TO_DO);
				}
			], function(err, result)
			{
				if(err) return callback(err);
				if(result == NOTHING_TO_DO) return callback(null, false);

				app.emit('database-loaded');
				app.io.emit('database-loaded');
				return callback(err, true);
			});
		}
	});
};

module.exports = function(model) { model.on('attached', function() { define(model); }); };