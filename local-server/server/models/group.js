var app = require('../server');
var async = require('async');
var loopback = require('loopback');
var remoteMethod = require('../system/helpers').remoteMethod;

function define(Group)
{
	function broadcastGroupChanges(ctx, next)
	{
		app.io.emit('groups-changed');
		next();
	}

	Group.observe('after delete', broadcastGroupChanges);
	Group.observe('after save', broadcastGroupChanges);
};

module.exports = function(Group) { Group.on('attached', function() { define(Group); }); };