var _ = require('underscore');
var async = require('async');
var loopback = require('loopback');
var disableAllMethods = require('../system/helpers').disableAllMethods;
var remoteMethod = require('../system/helpers').remoteMethod;
var app = require('../server');

var numberOfDevices = 0;
var lastNumberOfDevices = 0;
var defaultDeviceValue = 0;
var groupMap = {};

function define(Device)
{
	var DeviceSocket = require('../system/device-socket');
	var io = require('../server').io;

	var Group = loopback.getModel('Group');
	var Settings = loopback.getModel('Settings');
	var DeviceGroup = loopback.getModel('DeviceGroup');

	app.on('database-loaded', function()
	{
		Device.updateAll({ connected: false, currentValue: defaultDeviceValue });
	});

	DeviceGroup.find(null, function(err, deviceGroups)
	{
		if(err) return;

		groupMap = {};

		deviceGroups.forEach(function(deviceGroup)
		{
			groupMap[deviceGroup.deviceId] = deviceGroup.groupId;
		});
	});

	function numberOfDevicesChanged()
	{
		if(lastNumberOfDevices < numberOfDevices)
		{
			for(var i = lastNumberOfDevices + 1; i <= numberOfDevices; i++)
			{
				Device.create({ id: i });
			}
		}
		else
		{
			for(var i = lastNumberOfDevices; i > numberOfDevices; i--)
			{
				Device.destroyById(i);
			}
		}
	}

	function onDeviceCreated(device)
	{
		Device.emit('device-created', device);
		io.emit('device-created', device.toObject());
	}

	function onDeviceUpdated(device)
	{
		Device.emit('device-updated', device);
		io.emit('device-updated', device.toObject());
	}

	function onDeviceDestroyed(id)
	{
		Device.emit('device-destroyed', id);
		io.emit('device-destroyed', id);
	}

	Settings.on('change', function(result)
	{
		if(numberOfDevices != result.numberOfDevices)
		{
			lastNumberOfDevices = numberOfDevices;
			numberOfDevices = result.numberOfDevices;
			defaultDeviceValue = result.defaultDeviceValue;
			numberOfDevicesChanged();
		}
	});

	remoteMethod(Device,
	{
		name: 'resetValue',
		http: { verb: 'put' },
		returns: { type: 'boolean', root: true },
		definition: function(finalCallback)
		{
			io.emit('force-default-value', defaultDeviceValue);
			return finalCallback ? finalCallback(null, true) : null;
		}
	});

	Device.prototype.resetValue = function(socket)
	{
		socket.emit('force-default-value', defaultDeviceValue);
	};

	remoteMethod(Device,
	{
		name: 'enableDevices',
		http: { verb: 'put' },
		accepts: { arg: 'ids', type: ['number'], required: true },
		returns: { type: 'boolean', root: true },
		definition: function(ids, finalCallback)
		{
			var filter = { id: { inq: ids } };

			return async.waterfall(
			[
				function(next)
				{
					return Device.updateAll(filter, { enabled: true }, next);
				},
				function(count, next)
				{
					return Device.find({ where: filter }, next);
				},
				function(devices, next)
				{
					devices.forEach(onDeviceUpdated);
					return next(null, true);
				}
			], finalCallback);
		}
	});

	remoteMethod(Device,
	{
		name: 'disableDevices',
		http: { verb: 'put' },
		accepts: { arg: 'ids', type: ['number'], required: true },
		returns: { type: 'boolean', root: true },
		definition: function(ids, finalCallback)
		{
			var filter = { id: { inq: ids } };

			return async.waterfall(
			[
				function(next)
				{
					return Device.updateAll(filter, { enabled: false }, next);
				},
				function(count, next)
				{
					return Device.find({ where: filter }, next);
				},
				function(devices, next)
				{
					devices.forEach(onDeviceUpdated);
					return next(null, true);
				}
			], finalCallback);
		}
	});

	remoteMethod(Device,
	{
		name: 'addToGroup',
		http: { verb: 'put' },
		accepts:
		[
			{ arg: 'groupId', type: 'number' },
			{ arg: 'ids', type: ['number'] },
		],
		returns: { type: 'boolean', root: true },
		definition: function(groupId, ids, finalCallback)
		{
			var filter = { id: { inq: ids } };

			return async.waterfall(
			[
				function(next)
				{
					_.each(ids, function(id) { groupMap[id] = groupId; });
					return next(null, null);
				},
				function(count, next)
				{
					return Device.find({ where: filter }, next);
				},
				function(devices, next)
				{
					devices.forEach(onDeviceUpdated);
					DeviceGroup.persist(groupMap);
					return next(null, true);
				}
			], finalCallback);
		}
	});

	remoteMethod(Device,
	{
		name: 'removeFromGroup',
		http: { verb: 'put' },
		accepts: { arg: 'ids', type: ['number'], root: true },
		returns: { type: 'boolean', root: true },
		definition: function(ids, finalCallback)
		{
			var filter = { id: { inq: ids } };

			return async.waterfall(
			[
				function(next)
				{
					_.each(ids, function(id) { delete groupMap[id]; });
					return next(null, null);
				},
				function(count, next)
				{
					return Device.find({ where: filter }, next);
				},
				function(devices, next)
				{
					devices.forEach(onDeviceUpdated);
					DeviceGroup.persist(groupMap);
					return next(null, true);
				}
			], finalCallback);
		}
	});

	DeviceSocket.on('registration-attempt', function(id, callback)
	{
		Device.findById(id, function(err, device)
		{
			if(err) return callback(err);
			else if(!device) return callback('device not found');
			else if(device.connected) return callback('already connected');
			else return callback(null, device);
		});
	});

	DeviceSocket.on('connected', onDeviceUpdated);
	DeviceSocket.on('disconnect', onDeviceUpdated);
	DeviceSocket.on('value-change', onDeviceUpdated);
	
	Device.observe('after save', function(ctx, next)
	{
		if(ctx.isNewInstance)
		{
			return Device.findById(ctx.instance.id, function(err, device)
			{
				if(err) return next(err);

				onDeviceCreated(device);
				return next();
			});
		}
		else
		{
			return next();
		}
	});

	Device.observe('before delete', function(ctx, next)
	{
		onDeviceDestroyed(ctx.where.id);
		return next();
	});

	Group.observe('after delete', function(ctx, next)
	{
		var groups, devices;

		return async.series(
		[
			function(next)
			{
				return Group.find(function(err, result)
				{
					groups = _.indexBy(result, 'id');
					return next(err);
				});
			},
			function(next)
			{
				return Device.find(function(err, result)
				{
					devices = _.indexBy(result, 'id');
					return next(err);
				});
			},
			function(next)
			{
				var persistAfter = false;

				_.each(groupMap, function(groupId, deviceId)
				{
					if(groups[groupId]) return;
					
					persistAfter = true;
					delete groupMap[deviceId];
					onDeviceUpdated(devices[deviceId]);
				});

				if(persistAfter) DeviceGroup.persist(groupMap);
				return next();
			},
		], next);
	});
}

module.exports = function(Device)
{
	Device.on('attached', function()
	{
		app.on('started', function()
		{
			define(Device);
		});
	});
};