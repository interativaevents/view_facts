var _ = require('underscore');
var async = require('async');
var loopback = require('loopback');
var remoteMethod = require('../system/helpers').remoteMethod;

function define(DeviceGroup)
{
	var Device = loopback.getModel('Device');
	var persistTimeout;

	// disableAllMethods(DeviceGroup);

	DeviceGroup.persist = function(groupMap)
	{
		var io = require('../server').io;

		clearTimeout(persistTimeout);

		persistTimeout = setTimeout(function()
		{
			async.waterfall(
			[
				function(next) { DeviceGroup.destroyAll(null, next); },
				// function(count, next) { Device.find(null, next); },
				function(count, next)
				{
					return next(null, _.map(groupMap, function(groupId, deviceId)
					{
						return { groupId: groupId, id: deviceId };
					}));
				},
				function(devices, next)
				{
					var items = _.map(devices, function(device)
					{
						return { groupId: device.groupId, deviceId: device.id };
					});

					return DeviceGroup.create(items, next);
				},
				function(deviceGroups, next)
				{
					io.emit('device-groups-changed', _.map(deviceGroups, function(deviceGroup)
					{
						return deviceGroup.toObject();
					}));
					
					return next();
				}
			]);
		}, 80);
	};
}

module.exports = function(DeviceGroup) { DeviceGroup.on('attached', function() { define(DeviceGroup); }); };