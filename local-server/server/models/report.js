var _ = require('underscore');
var async = require('async');
var loopback = require('loopback');
var remoteMethod = require('../system/helpers').remoteMethod;

const NO_DATA = 'Sem Informação';

function parseSecond(totalSec)
{
	const minutes = parseInt(totalSec / 60);
	const seconds = totalSec % 60;

	return `${minutes < 10 ? '0' + minutes : minutes}:${seconds  < 10 ? '0' + seconds : seconds}`;
}

function sendCSV(res, name, content)
{
	var datetime = new Date();
	res.set('Expires', 'Tue, 03 Jul 2001 06:00:00 GMT');
	res.set('Cache-Control', 'max-age=0, no-cache, must-revalidate, proxy-revalidate');
	res.set('Last-Modified', datetime +'GMT');
	res.set('Content-Type','application/force-download');
	res.set('Content-Type','application/octet-stream');
	res.set('Content-Type','application/download');
	res.set('Content-Disposition','attachment;filename=' + name + '.csv');
	res.set('Content-Transfer-Encoding','binary');
	res.send(content);
}

function define(Report)
{
	var Group		= loopback.getModel('Group');
	var Record		= loopback.getModel('Record');
	var Activity	= loopback.getModel('Activity');
	var Breakpoint	= loopback.getModel('Breakpoint');
	var DeviceGroup	= loopback.getModel('DeviceGroup');

	remoteMethod(Report,
	{
		name: 'devices',
		http: { verb: 'get' },
		accepts: { arg: 'res', type: 'object', 'http': { source: 'res' } },
		returns: { type: 'string', root: true },
		definition: function(res, callback)
		{
			async.waterfall(
			[
				function(next)
				{
					Record.find(next);
				},
				function(records, next)
				{
					var duration = Activity.getDuration();
					var deviceIds = _.chain(records).pluck('deviceId').uniq().sort().value();
					var recordsByDevice = _.groupBy(records, 'deviceId');
					var csv = [];

					csv.push(['Tempo'].concat(deviceIds.map(function(id) { return 'Device #' + id }).join(',')));

					for(var i = 0; i <= duration; i++)
					{
						var line = [parseSecond(i)];

						for(var j = 0; j < deviceIds.length; j++)
						{
							var deviceId = deviceIds[j];
							var record = _.findWhere(recordsByDevice[deviceId], { second: i });

							line.push(record ? record.value : NO_DATA);
						}

						csv.push(line.join(','));
					}

					next(null, csv.join('\n'));
				}
			], function(err, result)
			{
				if(err) return callback(err);
				return sendCSV(res, 'relatorio-por-device', result);
			});
		}
	});

	remoteMethod(Report,
	{
		name: 'groupAverage',
		http: { verb: 'get' },
		accepts: { arg: 'res', type: 'object', 'http': { source: 'res' } },
		returns: { type: 'string', root: true },
		definition: function(res, callback)
		{
			async.waterfall(
			[
				function(next)
				{
					async.parallel(
					[
						function(next) { Group.find(next); },
						function(next) { Record.find(next); },
						function(next) { DeviceGroup.find(next); }
					], next);
				},
				function(results, next)
				{
					var groups			= results[0] || [];
					var records			= results[1];
					var deviceGroups	= results[2];

					if(groups.length == 0)
					{
						groups = [{ id: -1, name: 'Nenhum Grupo' }];
					}

					var csv				= [];
					var duration		= Activity.getDuration();
					var groupMap		= _.indexBy(groups, 'id');
					var groupIds		= _.pluck(groups, 'id');
					var deviceGroupMap	= _.indexBy(deviceGroups, 'deviceId');

					var recordsByGroup	= {};

					_.each(records, function(record)
					{
						var deviceGroup	= deviceGroupMap[record.deviceId];
						var groupId		= deviceGroup ? deviceGroup.groupId : -1;
						var entry		= recordsByGroup[groupId] = recordsByGroup[groupId] || [];
						entry.push(record);
					});

					csv.push(['Tempo'].concat(_.pluck(groups, 'name').join(',')));

					for(var i = 0; i <= duration; i++)
					{
						var line = [parseSecond(i)];

						for(var j = 0; j < groupIds.length; j++)
						{
							var sum = 0;
							var deviceTotal = 0;
							var groupId = groupIds[j];
							var records = _.where(recordsByGroup[groupId], { second: i });

							_.each(records, function(record)
							{
								sum += record.value;
								deviceTotal += 1;
							});

							line.push(deviceTotal ? Math.round(sum / deviceTotal) : NO_DATA);
						}

						csv.push(line.join(','));
					}

					next(null, csv.join('\n'));
				}
			], function(err, result)
			{
				if(err) return callback(err);
				return sendCSV(res, 'relatorio-media-por-grupo', result);
			});
		}
	});

	remoteMethod(Report,
	{
		name: 'breakpoint',
		http: { verb: 'get' },
		accepts: { arg: 'res', type: 'object', 'http': { source: 'res' } },
		returns: { type: 'string', root: true },
		definition: function(res, callback)
		{
			async.waterfall(
			[
				function(next)
				{
					async.parallel(
					[
						function(next) { Group.find(next); },
						function(next) { Record.find(next); },
						function(next) { Breakpoint.find(next); },
						function(next) { DeviceGroup.find(next); }
					], next);
				},
				function(results, next)
				{
					var groups			= results[0] || [];
					var records			= results[1];
					var deviceGroups	= results[3];

					if(groups.length == 0)
					{
						groups = [{ id: -1, name: 'Nenhum Grupo' }];
					}

					var duration		= Activity.getDuration();
					var groupMap		= _.indexBy(groups, 'id');
					var groupIds		= _.pluck(groups, 'id');
					var deviceGroupMap	= _.indexBy(deviceGroups, 'deviceId');
					var breakpoints		= _.uniq([0].concat(_.pluck(results[2], 'second')).concat([duration + 1]));

					var recordsByGroup	= {};

					_.each(records, function(record)
					{
						var deviceGroup	= deviceGroupMap[record.deviceId];
						var groupId		= deviceGroup ? deviceGroup.groupId : -1;
						var entry		= recordsByGroup[groupId] = recordsByGroup[groupId] || [];
						entry.push(record);
					});

					var averagePerGroupPerTime = {};

					for(var i = 0; i <= duration; i++)
					{
						var timeEntry = averagePerGroupPerTime[i] = {};

						for(var j = 0; j < groupIds.length; j++)
						{
							var sum = 0;
							var deviceTotal = 0;
							var groupId = groupIds[j];
							var records = _.where(recordsByGroup[groupId], { second: i });

							_.each(records, function(record)
							{
								sum += record.value;
								deviceTotal += 1;
							});

							if(deviceTotal)
							{
								timeEntry[groupId] = Math.round(sum / deviceTotal);
							}
						}
					}

					var averagePerGroupPerBreakpoint = {};
					var lastBreakpoint;

					for(var i = 0; i < breakpoints.length; i++)
					{
						var breakpoint = breakpoints[i];
						var key = lastBreakpoint + '-' + (breakpoint - 1);

						if(lastBreakpoint != null)
						{
							var entry = averagePerGroupPerBreakpoint[key] = [];

							for(var j in averagePerGroupPerTime)
							{
								if(j >= lastBreakpoint && j <= breakpoint)
								{
									entry.push(averagePerGroupPerTime[j]);
								}
							}
						}

						lastBreakpoint = breakpoint;
					}

					var maxAndMinValuesPerBreakpointPerGroup = {};

					for(var breakpoint in averagePerGroupPerBreakpoint)
					{
						var entry = maxAndMinValuesPerBreakpointPerGroup[breakpoint] = {};
						var averagePerGroup = {}; _.each(averagePerGroupPerBreakpoint[breakpoint], function(item)
						{
							for(var groupId in item)
							{
								(averagePerGroup[groupId] = averagePerGroup[groupId] || []).push(item[groupId]);
							}
						});

						for(var groupId in averagePerGroup)
						{
							entry[groupId] =
							{
								max: _.max(averagePerGroup[groupId]),
								min: _.min(averagePerGroup[groupId])
							};
						}
					}

					var csv = [];
					var header = ['Tempo'];

					_.each(groups, function(group)
					{
						header.push(group.name + ' - Mínima');
						header.push(group.name + ' - Máxima');
					});

					csv.push(header.join(','));

					for(var breakpoint in maxAndMinValuesPerBreakpointPerGroup)
					{
						var line	= [];
						var start	= breakpoint.split('-')[0];
						var end		= breakpoint.split('-')[1];
						var label	= parseSecond(start) + ' - ' + parseSecond(end);

						var breakpointInfo = maxAndMinValuesPerBreakpointPerGroup[breakpoint];
						var groupInfo = breakpointInfo ? breakpointInfo[groupId] : null;

						line.push(label);

						_.each(groupIds, function(groupId)
						{
							line.push(groupInfo ? groupInfo.min : NO_DATA);
							line.push(groupInfo ? groupInfo.max : NO_DATA);
						});

						csv.push(line.join(','));
					}

					next(null, csv.join('\n'));
				}
			], function(err, result)
			{
				if(err) return callback(err);
				return sendCSV(res, 'relatorio-minima-e-maxima-por-breakpoint', result);
			});
		}
	});
};

module.exports = function(Report) { Report.on('attached', function() { define(Report); }); };