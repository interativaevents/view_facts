module.exports = function(server)
{
	var router = server.loopback.Router();
	var redirect = function(path) { return function(req, res) { res.redirect(path); } };

	router.get('/admin', redirect('/admin.html'));
	router.get('/keypad', redirect('/keypad.html'));
	router.get('/status.json', redirect('/assets/status.json'));
	
	server.use(router);
};