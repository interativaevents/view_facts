var fs = require('fs');
var net = require('net');
var async = require('async');
var path = require('path');
var loopback = require('loopback');
var boot = require('loopback-boot');

var app = loopback();

app.use(loopback.static(path.resolve(__dirname, '../public')));

function checkPort(port, callback)
{
	var server = net.createServer();

	server.once('error', function(err)
	{
		return callback(err);
	});

	server.once('listening', function()
	{
		server.close();
		return callback();
	});

	return server.listen(port);
}

app.start = function(port, callback)
{
	async.series(
	[
		function(next)
		{
			if(!port) return next('PORT_UNDEFINED');
			return checkPort(port, next);
		},
		function(next)
		{
			return boot(app, __dirname, next);
		},
		function(next)
		{
			var server = app.listen(port, function(err)
			{
				if(err) return next(err);
				app.io = require('socket.io')(server);
				app.emit('started');
				return next();
			});
		}
	], callback);
};

module.exports = app;