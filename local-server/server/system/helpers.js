var loopback = require('loopback');

module.exports.disableAllMethods = function disableAllMethods(model, methodsToExpose)
{
	if(model && model.sharedClass)
	{
		methodsToExpose = methodsToExpose || [];

		var modelName = model.sharedClass.name;
		var methods = model.sharedClass.methods();
		var relationMethods = [];
		var hiddenMethods = [];

		try
		{
			Object.keys(model.definition.settings.relations).forEach(function(relation)
			{
				relationMethods.push({ name: '__findById__' + relation, isStatic: false });
				relationMethods.push({ name: '__destroyById__' + relation, isStatic: false });
				relationMethods.push({ name: '__updateById__' + relation, isStatic: false });
				relationMethods.push({ name: '__exists__' + relation, isStatic: false });
				relationMethods.push({ name: '__link__' + relation, isStatic: false });
				relationMethods.push({ name: '__get__' + relation, isStatic: false });
				relationMethods.push({ name: '__create__' + relation, isStatic: false });
				relationMethods.push({ name: '__update__' + relation, isStatic: false });
				relationMethods.push({ name: '__destroy__' + relation, isStatic: false });
				relationMethods.push({ name: '__unlink__' + relation, isStatic: false });
				relationMethods.push({ name: '__count__' + relation, isStatic: false });
				relationMethods.push({ name: '__delete__' + relation, isStatic: false });
			});
		} catch(err) {}

		methods.concat(relationMethods).forEach(function(method)
		{
			var methodName = method.name;
			if(methodsToExpose.indexOf(methodName) < 0)
			{
				hiddenMethods.push(methodName);
				model.disableRemoteMethod(methodName, method.isStatic);
			}
		});

		if(hiddenMethods.length > 0)
		{
			// console.log('\nRemote mehtods hidden for', modelName, ':', hiddenMethods.join(', '), '\n');
		}
	}
};

module.exports.remoteMethod = function remoteMethod(model, config)
{
	if(config.isStatic === false)
	{
		model.prototype[config.name] = config.definition;
	}
	else
	{
		model[config.name] = config.definition;
	}

	model.remoteMethod(config.name, config);
};

module.exports.isDefined = function isDefined(value)
{
	return !(value === undefined || value === null);
};

// module.exports.query = function query(sql, callback)
// {
// 	var app = require('../server');
// 	return app.dataSources.sqlite.connector.query(sql, callback);
// };