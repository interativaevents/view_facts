var loopback = require('loopback');
var TinyEmitter = require('tiny-emitter');
var io = require('../server').io;
var emitter = new TinyEmitter();

var sockets = {};
var Device;

io.on('connection', function(socket)
{
	socket.on('register-device', function(id, callback)
	{
		emitter.emit('registration-attempt', id, function(err, device)
		{
			if(err) return callback(err);

			sockets[id] = socket;

			new DeviceSocket(id, socket);
			return callback(null, id);
		});
	});
});

function updateAttribute(id, attribute, value, callback)
{
	if(!Device) Device = loopback.getModel('Device');

	Device.findById(id, function(err, device)
	{
		if(err) return callback(err);
		
		return device.updateAttribute(attribute, value, function(err)
		{
			if(err) return callback(err);
			return callback(null, device);
		});
	});
}

function DeviceSocket(deviceId, socket)
{
	this.deviceId = deviceId;
	this.socket = socket;

	socket.on('value-change', this.onValueChange.bind(this));
	socket.on('disconnect', this.onDisconnect.bind(this));

	this.onConnect();
}

DeviceSocket.prototype.onConnect = function()
{
	var socket = this.socket;
	
	updateAttribute(this.deviceId, 'connected', true, function(err, device)
	{
		if(err) return;
		emitter.emit('connected', device);
		device.resetValue(socket);
	});
};

DeviceSocket.prototype.onDisconnect = function()
{
	updateAttribute(this.deviceId, 'connected', false, function(err, device)
	{
		if(!err) emitter.emit('disconnect', device);
	});
};

DeviceSocket.prototype.onValueChange = function(value)
{
	updateAttribute(this.deviceId, 'currentValue', value, function(err, device)
	{
		if(!err) emitter.emit('value-change', device);
	});
};

module.exports = emitter;