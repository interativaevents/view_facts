var version = '1.4';
var requestTimeout = 500;
var concurrentRequests = 6;
var serverSuffixes = _.range(1, 255);
var serverPrefixes = 
[
	//Mais utilizados
	'192.168.0.',
	'10.11.1.',
	'192.168.1.',
	'10.11.0.',

	//Outras opções comuns
	'10.0.0.',
	'10.0.1.',
	'10.1.1.',
	'10.1.10.',
	'10.90.90.',
	'192.168.10.',
	'192.168.100.',
	'192.168.102.',
	'192.168.11.',
	'192.168.123.',
	'192.168.15.',
	'192.168.16.',
	'192.168.168.',
	'192.168.2.',
	'192.168.20.',
	'192.168.223.',
	'192.168.251.',
	'192.168.254.',
	'192.168.3.',
	'192.168.30.',
	'192.168.4.',
	'192.168.50.',
	'192.168.55.',
	'192.168.62.',
	'192.168.8.',
	'200.200.200.'
];

function setView(name)
{
	$('.view').removeClass('active');
	$('#view-' + name).addClass('active');
}

function testHost(host, callback)
{
	$.ajax(
	{
		url: 'http://' + host + '/status.json',
		dataType: 'json',
		timeout: requestTimeout,
		error: function(xhr, errorType, error)
		{
			return callback(errorType);
		},
		success: function(data, status)
		{
			return callback(null, data);
		}
	});
}

function connect(host)
{
	window.location.href = 'http://' + host + '/keypad';
}

function enterServerAddress()
{
	var host = prompt('Digite o endereço do servidor no formato "ip:porta"');

	if(host)
	{
		return testHost(host, function(err, success)
		{
			if(success) return connect(host);
			return alert('Servidor não encontrado.');
		});
	}
}

var searchInProgress = false;
function updateProgress(progress, total)
{
	document.getElementById('progress').innerHTML = Math.round(progress * 100 / total) + '%';
}

function findServer()
{
	if(searchInProgress) return;

	/*
	 * We treat error as success, and success as error
	 * because we want execution to stop when it finds the server
	 */

	var port = prompt('Em qual porta o servidor está aguardando conexões?');

	if(!port) return;

	searchInProgress = true;
	setView('find-server');

	var counter = 0;
	var calls = _.chain(serverPrefixes).map(function(prefix)
	{
		return _.map(serverSuffixes, function(suffix)
		{
			var host = prefix + suffix + ':' + port;

			return function(next)
			{
				if(!searchInProgress) return next(null, 'aborted');

				return testHost(host, function(err, success)
				{
					updateProgress(++counter, calls.length);
					if(success) return next(host);
					return next();
				});
			};
		});
	}).flatten().value();

	updateProgress(counter, calls.length);

	return async.parallelLimit(calls, concurrentRequests, function(host, err)
	{
		if(host) return connect(host);
		if(err != 'aborted') alert('Servidor não encontrado.');
		return abortServerSearch(true);
	});
}

function abortServerSearch(skipConfirmation)
{
	if(!skipConfirmation && !confirm('Você está certo de que deseja cancelar a busca?'))
	{
		return;
	}

	searchInProgress = false;
	setView('home');
}

if(window.navigator.standalone)
{
	setView('home');
}
else
{
	setView('install-screen');
}

document.body.ontouchmove = function(e){ e.preventDefault(); };
document.getElementById('version-info').innerHTML = 'v' + version;