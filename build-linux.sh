mkdir build
rm -rf build/viewfacts-linux-x64
rm -rf temp
cd client
npm install
gulp build -p
cd ..
cp -R electron temp
cp -R local-server temp/
cd temp
npm install
cd local-server
npm install
cd ..
./node_modules/.bin/electron-packager . viewfacts --overwrite --platform=linux --arch=x64 --asar --prune=true
cd ..
cp -R temp/viewfacts-linux-x64 build/viewfacts-linux-x64
rm -rf temp