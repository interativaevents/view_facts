mkdir build
rm -rf build/viewfacts-darwin-x64
rm -rf temp
cd client
npm install
gulp build -p
cd ..
cp -R electron temp
cp -R local-server temp/
cd temp
npm install
cd local-server
npm install
cd ..
./node_modules/.bin/electron-packager . viewfacts --platform=darwin --arch=x64 --asar --prune=true --overwrite
cd ..
cp -R temp/viewfacts-darwin-x64 build/viewfacts-darwin-x64
rm -rf temp