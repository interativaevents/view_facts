module.exports =
{
	src: './src',
	dest: '../local-server/public',
	html: ['admin.html', 'admin-second-screen.html', 'keypad.html'],
	styles: ['admin.less', 'keypad.less'],
	javascript:
	{
		entry:
		{
			'admin': 'admin/main.js',
			'admin-second-screen': 'admin/main-second-screen.js',
			'keypad': 'keypad/main.js'
		},
		outputName: '[name].js',	
	},
};