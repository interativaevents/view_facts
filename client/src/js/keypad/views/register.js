let lastUsedId = window.localStorage.getItem('lastUsedId') || '';

export default function Register(settings)
{
	let el, input, button;

	el = document.createElement('div');
	el.innerHTML = `<div class="register-view">
						<img src="/assets/Logo_interativa.png">
						<div id="register-message">Insira o número do seu dispositivo</div>
						<input id="register-input" type="number" value="${lastUsedId}" />
						<span id="register-button" class="btn btn-pink">Clique para conectar!</span>
					</div>`;

	button = el.querySelector('#register-button');
	input = el.querySelector('input');

	function addListeners()
	{
		button.onclick = function(event)
		{
			event.preventDefault();
			settings.onRegister(input.value);
		};

		document.body.ontouchmove = function(e)
		{
			e.preventDefault();
		};
	}

	function removeListeners()
	{
		button.onclick = null;
		document.body.ontouchmove = null;
	}
	
	this.appendTo = function(node)
	{
		node.appendChild(el);
		addListeners();
	};

	this.remove = function()
	{
		removeListeners();
		el.parentNode.removeChild(el);
	};
}