export default function Offline()
{
	let el = document.createElement('div');
	el.innerHTML = `<div class="offline-view">
						<div class="offline-wireframe">
							<p>Conexão interrompida!</p>
						<div id="score"><img src="/assets/layerbranco.png"/></div>
						</div>	
					</div>`;

	this.appendTo = function(node)
	{
		document.body.ontouchmove = function(e){ e.preventDefault(); };
		node.appendChild(el);
	};

	this.remove = function()
	{
		document.body.ontouchmove = null;
		el.parentNode.removeChild(el);
	};
}