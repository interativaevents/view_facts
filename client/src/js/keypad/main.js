import socket from '../common/socket-io';

import Offline from './views/offline';
import Register from './views/register';
import Slider from './views/slider';

const offlineView	= new Offline();
const registerView	= new Register({ onRegister: onRegister });
const sliderView	= new Slider({ onChange: onChange });
const node			= document.getElementById('app');

let forceRefresh = false;
let currentView;

var dateInit = Date.now();
function onRegister(id)
{
	socket.emit('register-device', id, function(err)
	{
		if(err) return console.log(err);
		window.localStorage.setItem('lastUsedId', id);
		return render(true, id);
	});
}

function onChange(value)
{
	if(dateInit<Date.now(-300)){
		dateInit=Date.now();
		socket.emit('value-change', value);
	}
}

socket.on('connect', function()
{
	if(forceRefresh)
	{
		window.location.reload();
	}

	render(true);
});

socket.on('disconnect', function()
{
	forceRefresh = true;
	render(false);
});

socket.on('device-destroyed', function()
{
	render(true);
});

function render(online, id, value)
{
	let newView;

	// if(!online) newView = offlineView;
	if(!online) newView = registerView;
	else if(id) newView = sliderView;
	else newView = registerView;

	if(currentView) currentView.remove();

	newView.appendTo(node);

	currentView = newView;
	
}