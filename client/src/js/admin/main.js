import React from 'react';
import { render } from 'react-dom';
import { Router, Route, useRouterHistory } from 'react-router';
import createHashHistory from 'history/lib/createHashHistory';

import socket from '../common/socket-io';

import App from './app';
import Reports from './pages/reports';
import Activity from './pages/activity';
import Playback from './pages/playback';
import Settings from './pages/settings';
import Groups from './pages/groups';
import ConnectedDevices from './pages/connected-devices';

const appHistory = top.appHistory = useRouterHistory(createHashHistory)({ queryKey: false });

socket.on('database-loaded', function()
{
	window.location.reload();
});

render((
	<Router history={appHistory}>
		<Route component={App}>
			<Route path="/" component={Activity} />
			<Route path="playback" component={Playback} />
			{/*<Route path="connected-devices" component={ConnectedDevices} />*/}
			{/*<Route path="reports" component={Reports} />*/}
			{/*<Route path="groups" component={Groups} />*/}
			<Route path="settings" component={Settings} />
		</Route>
	</Router>
), document.getElementById('app'));