import React from 'react';
import LiLink from './components/li-link';
import serverAddress from '../common/server-address';

export default (props) => (
	<div>
		<ul className="main-menu nav nav-tabs">
			<LiLink activeClassName="active" to="/">Atividade</LiLink>
			<LiLink activeClassName="active" to="/playback">Playback</LiLink>
			{/*<LiLink activeClassName="active" to="/connected-devices">Lista de Dispositivos</LiLink>*/}
			{/*<LiLink activeClassName="active" to="/groups">Grupos</LiLink>*/}
			{/*<LiLink activeClassName="active" to="/reports">Relatórios</LiLink>*/}
			<LiLink activeClassName="active" to="/settings">Configurações</LiLink>
		</ul>
		<div className="keypad-address">Endereço do keypad: <b>{`${serverAddress()}/keypad`}</b></div>
		<div className="page">{props.children}</div>
	</div>
);