import React, { Component } from 'react';
import _ from 'underscore';
import { connect } from '../myflux';
import DeviceStore from '../stores/device';
import ActivityStore from '../stores/activity';
import ActivityChart from '../components/activity-chart';

import ConnectedDevices from './connected-devices';

import dialog from '../dialog';

function openSecondScreen()
{
	window.open('/admin-second-screen.html', null, 'width=1110,height=510');
}

class ActivityPage extends Component
{
	constructor(props)
	{
		super(props);
		this.disposables = [];
		this.state = { activityStream: null };
	}

	componentDidMount()
	{
		this.disposables.push(ActivityStore.onFirstFetch(this.onActivityFetched.bind(this)));
		this.disposables.push(ActivityStore.on('clear', this.onActivityCleared.bind(this)));
	}

	componentWillUnmount()
	{
		this.disposables.forEach(func => func());
	}

	onActivityFetched()
	{
		const activityStream = ActivityStore.getStream();
		this.setState({ activityStream });
	}

	onActivityCleared()
	{
		if(this.refs && this.refs.activityChart)
		{
			this.refs.activityChart.clear();
		}
	}

	resetValue()
	{
		DeviceStore.resetValue();
	}

	startActivity()
	{
		ActivityStore.start();
	}

	stopActivity()
	{
		ActivityStore.stop();
	}

	clearActivity()
	{
		dialog.confirm('Você está certo de que deseja limpar o banco de dados? Todos os dados serão perdidos.', function(err, result)
		{
			if(result == 'no') return;
			ActivityStore.clear();
		});
	}

	newBreakpoint()
	{
		ActivityStore.breakpoint();
	}

	render()
	{
		const { activityStream } = this.state;
		const running = ActivityStore.isRunning();
		
		if(activityStream)
		{		
			return (
				<div>
					<div className="text-right">
						<button
							onClick={openSecondScreen}
							className="btn btn-default btn-xs">Abrir segunda tela</button>
						&nbsp;
						{/*<button
							onClick={this.resetValue.bind(this)}
							className="btn btn-default btn-xs">Resetar dispositivos</button>
						&nbsp;*/}
						<div className="btn-group">
							<button
								disabled={running}
								onClick={this.startActivity.bind(this)}
								className="btn btn-default btn-xs">Iniciar</button>
							<button
								disabled={!running}
								onClick={this.stopActivity.bind(this)}
								className="btn btn-default btn-xs">Parar</button>
							<button
								disabled={!running}
								onClick={this.newBreakpoint.bind(this)}
								className="btn btn-default btn-xs">Breakpoint</button>
						</div>
						&nbsp;
						<button
							disabled={running}
							onClick={this.clearActivity.bind(this)}
							className="btn btn-default btn-xs">Limpar</button>
					</div>
					<hr />
					<ActivityChart ref="activityChart" stream={activityStream} />
					{/*Alteraçoes*/}
					<hr />
					<div style={{ padding: '15px 15px 0' }}>
						<ConnectedDevices />
					</div>

				</div>
			);
		}

		return null;
	}
}

export default connect(ActivityStore)(ActivityPage);