import React, { Component } from 'react';
import _ from 'underscore';
import { connect } from '../myflux';
import GroupStore from '../stores/group';

class GroupPage extends Component
{
	create(event)
	{
		if(event) event.preventDefault();
		const group = {};
		const name = this.refs.name.value;
		if(!name) return;

		group.name = name;
		this.refs.name.value = '';
		GroupStore.save(group);
	}

	update(id, event)
	{
		if(event) event.preventDefault();
		const group = GroupStore.getById(id);
		const name = prompt(`Digite o novo nome do grupo "${group.name}".`, group.name);
		if(!name) return;

		group.name = name;
		GroupStore.save(group);
	}

	destroy(id, event)
	{
		if(event) event.preventDefault();
		const group = GroupStore.getById(id);
		
		if(confirm(`Você está certo de que deseja remover o grupo "${group.name}"?`))
		{
			GroupStore.destroy(id);
		}
	}

	render()
	{
		const groups = GroupStore.get().filter((group) => group.id != -1);
		const renderGroup = (group) =>
		{
			return	<li className="list-group-item clearfix" key={group.id}>
						<div className="pull-left" style={{marginTop: 6}}>{group.name}</div>
						<div className="btn-group pull-right">
							<button className="btn btn-default" onClick={this.update.bind(this, group.id)}>Editar</button>
							<button className="btn btn-default" onClick={this.destroy.bind(this, group.id)}>Remover</button>
						</div>
					</li>;
		};

		return (
			<div>
				<div className="form-group">
					<label>Novo Grupo</label>
					<div className="input-group">
						<input className="form-control" ref="name" />
						<div className="input-group-btn">
							<button className="btn btn-primary" onClick={this.create.bind(this)}>Adicionar</button>
						</div>
					</div>
				</div>
				
				<ul className="list-group">
				{groups.map(renderGroup)}
				</ul>
			</div>
		);
	}
}

export default connect(GroupStore)(GroupPage);