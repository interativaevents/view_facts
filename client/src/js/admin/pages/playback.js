import React, { Component } from 'react';
import ActivityStore from '../stores/activity';
import ActivityChart from '../components/activity-chart';

export default class PlaybackPage extends Component
{
	constructor(props)
	{
		super(props);
		this.disposables = [];
		this.state = { playback: null, playbackStream: null };
	}

	componentDidMount()
	{
		this.disposables.push(ActivityStore.onFirstFetch(this.onActivityFetched.bind(this)));
	}

	componentWillUnmount()
	{
		this.disposables.forEach(func => func());
	}

	onActivityFetched()
	{
		const playback = ActivityStore.newPlayback();
		const playbackStream = playback.getStream();
		const onPlaybackStatusChange = () => this.forceUpdate();

		playback.on('status-change', onPlaybackStatusChange);

		this.disposables.push(() => playback.off('status-change', onPlaybackStatusChange));
		this.disposables.push(() => playback.dispose());

		this.setState({ playback, playbackStream });
	}

	render()
	{
		const { playback, playbackStream } = this.state;
		
		if(playback && playbackStream)
		{
			const status = playback.getStatus();
			const playing = status == 'PLAYING';
			const completed = playing == 'COMPLETED';

			return (
				<div>
					<div className="text-right">
						<div className="btn-group">
							<button
								disabled={playing}
								onClick={() => playback.play()}
								className="btn btn-default btn-xs">{completed ? 'Reiniciar' : 'Play'}</button>
							<button
								disabled={!playing}
								onClick={() => playback.pause()}
								className="btn btn-default btn-xs">Pause</button>
						</div>
					</div>
					<hr />
					<ActivityChart hideDeviceCount={true} stream={playbackStream} />
				</div>
			);
		}

		return null;
	}
}