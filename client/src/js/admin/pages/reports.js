import React, { Component } from 'react';

export default class ReportsPage extends Component
{
	render()
	{
		return (
			<ul>
				<li><a href="/api/Reports/devices">Baixar relatório por device</a></li>
				<li><a href="/api/Reports/groupAverage">Baixar relatório por grupo</a></li>
				<li><a href="/api/Reports/breakpoint">Baixar relatório por breakpoint</a></li>
			</ul>
		);
	}
};