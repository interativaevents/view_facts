import _ from 'underscore';
import React, { Component } from 'react';
import { findDOMNode } from 'react-dom';
import { connect } from '../myflux';
import DeviceStore from '../stores/device';
import GroupStore from '../stores/group';

class Device extends Component
{
	onDivClick(e)
	{
		this.props.onClick(true);
	}

	onCheckboxClick(e)
	{
		e.stopPropagation();
		this.props.onClick(false);
	}

	render()
	{
		const { props } = this;
		const group = GroupStore.getById(props.device.groupId);

		return (
			<div className={'device' + (props.selected ? ' device-selected' : '') } onClick={this.onDivClick.bind(this)}>
				<div className="device-device-number"><input type="checkbox" checked={props.selected} onClick={this.onCheckboxClick.bind(this)} /> Device #{props.device.id}</div>
				<div className="device-current-value">
					<span className="label label-default">{props.device.currentValue}%</span>
				</div>
				<div className="device-group">
				{
					group ?
					<span className="label label-warning">{group.name}</span> :
					<span className="label label-default">Sem Grupo</span>
				}
				</div>
				<div className="device-connected-label">
				{
					props.device.connected ?
					<span className="label label-success">Conectado</span> :
					<span className="label label-default">Não Conctado</span>
				}
				</div>
				<div className="device-enabled-label">
				{
					props.device.enabled ?
					<span className="label label-warning">Habilitado</span> :
					<span className="label label-danger">Desabilitado</span>
				}
				</div>
			</div>
		);
	}
}

Device.defaultProps =
{
	selected: false,
	onClick: function(){}
};

class MouseAreaSelector extends Component
{
	constructor(props)
	{
		super(props);
		this.state = { startX: 0, startY: 0, endX: 0, endY: 0 };
		this.el = document.createElement('div');
		this.el.className = 'mouse-area-selector';
	}

	componentDidMount()
	{
		document.body.appendChild(this.el);

		this.__dragging = false
		this.__onMouseDownHandler = this.onMouseDown.bind(this);
		this.__onMouseUpHandler = this.onMouseUp.bind(this);
		this.__onMouseMoveHandler = this.onMouseMove.bind(this);

		document.body.addEventListener('mousedown', this.__onMouseDownHandler);
		document.body.addEventListener('mouseup', this.__onMouseUpHandler);
		document.body.addEventListener('mousemove', this.__onMouseMoveHandler);
	}

	onMouseDown()
	{
		this.__dragging = true;
		this.setState({ startX: document.body.scrollLeft + event.clientX, startY: document.body.scrollTop + event.clientY });
		document.body.style['-webkit-user-select'] = 'none';
	}

	onMouseUp()
	{
		this.__dragging = false;
		this.setState({ startX: 0, startY: 0, endX: 0, endY: 0 });
		document.body.style['-webkit-user-select'] = 'auto';
	}

	onMouseMove(event)
	{
		if(this.__dragging)
		{
			this.setState({ endX: document.body.scrollLeft + event.clientX, endY: document.body.scrollTop + event.clientY });
			this.props.onSelecting(_.clone(this.state), this.el);
		}
	}

	componentWillUnmount()
	{
		document.body.removeChild(this.el);
		document.body.removeEventListener('mousedown', this.__onMouseDownHandler);
		document.body.removeEventListener('mouseup', this.__onMouseUpHandler);
		document.body.removeEventListener('mousemove', this.__onMouseMoveHandler);
	}

	render()
	{
		const { startX, startY, endX, endY } = this.state;

		if(!this.__dragging)
		{
			this.el.style.left = '-10px';
			this.el.style.top = '-10px';
			this.el.style.width = '0';
			this.el.style.height = '0';
		}
		else if(endX > 0 && endY > 0)
		{
			const minX = Math.min(startX, endX);
			const minY = Math.min(startY, endY);
			const maxX = Math.max(startX, endX);
			const maxY = Math.max(startY, endY);

			this.el.style.left = minX + 'px';
			this.el.style.top = minY + 'px';

			this.el.style.width = (maxX - minX) + 'px';
			this.el.style.height = (maxY - minY) + 'px';
		}

		return null;
	}
};

MouseAreaSelector.propTypes =
{
	onSelecting: React.PropTypes.func
};

class DeviceList extends Component
{
	constructor(props)
	{
		super(props);
		this.state = { selectedDevicesMap: {}, groupDropdownOpened: false };
	}

	getSelectedDevices()
	{
		const { selectedDevicesMap } = this.state, selectedDevices = []; 
		_.each(this.state.selectedDevicesMap, (selected, deviceId) => { if(selected) selectedDevices.push(deviceId) } );
		return selectedDevices;
	}

	onDeviceClicked(device, selectOnly)
	{
		let { selectedDevicesMap } = this.state;
		let { id } = device;
		
		if(selectOnly)
		{
			this.setState({ selectedDevicesMap: { [id]: !selectedDevicesMap[id] } });
		}
		else
		{
			this.setState({ selectedDevicesMap: {...selectedDevicesMap, [id]: !selectedDevicesMap[id]} });
		}
	}

	onMouseSelecting(coordinates, selectorNode)
	{
		let selectedDevicesMap = {};
		let devices = DeviceStore.get();
		let selectorRect = selectorNode.getBoundingClientRect();

		_.each(devices, (device) =>
		{
			let node = findDOMNode(this.refs['device-' + device.id]);
			let rect = node.getBoundingClientRect();
			let overlap = !(selectorRect.right < rect.left || 
							selectorRect.left > rect.right || 
							selectorRect.bottom < rect.top || 
							selectorRect.top > rect.bottom);

			if(overlap)
			{
				selectedDevicesMap[device.id] = true;
			}
		});

		this.setState({ selectedDevicesMap });
	}

	enableDevice()
	{
		const selectedDevices = this.getSelectedDevices();
		if(_.isEmpty(selectedDevices)) return;
		
		DeviceStore.enableDevice(selectedDevices);
	}

	disableDevice()
	{
		const selectedDevices = this.getSelectedDevices();
		if(_.isEmpty(selectedDevices)) return;
		
		DeviceStore.disableDevice(selectedDevices);
	}

	addDevicesToGroup(groupId, event)
	{
		const selectedDevices = this.getSelectedDevices();

		if(event) event.preventDefault();
		if(_.isEmpty(selectedDevices)) return;
		
		DeviceStore.addToGroup(groupId, selectedDevices);
		this.setState({ groupDropdownOpened: false });
	}

	removeDevicesFromGroup(event)
	{
		const selectedDevices = this.getSelectedDevices();

		if(event) event.preventDefault();
		if(_.isEmpty(selectedDevices)) return;
		
		DeviceStore.removeDevices(selectedDevices);
		this.setState({ groupDropdownOpened: false });
	}

	render()
	{
		let { selectedDevicesMap, groupDropdownOpened } = this.state;
		let devices = DeviceStore.get();
		let groups = GroupStore.get();
		let groupSelector;

		if(!_.isEmpty(groups))
		{
			groupSelector = (
				<div className={'btn-group' + (groupDropdownOpened ?' open' : '')}>
					<button
						onClick={() => this.setState({ groupDropdownOpened: !groupDropdownOpened })}
						className="btn btn-xs btn-default dropdown-toggle">Alterar Grupo <span className="caret" /></button>
					<ul className="dropdown-menu dropdown-menu-right">
						<li><a href="#" onClick={this.removeDevicesFromGroup.bind(this)}>Nenhum</a></li>
						<li role="separator" className="divider"></li>
						{groups.filter(group => group.id != -1).map((group) =>(
							<li key={group.id}><a href="#" onClick={this.addDevicesToGroup.bind(this, group.id)}>{group.name}</a></li>
						))}
					</ul>
				</div>
			);
		}

		return	<div className="device-list">
					<div className="text-right">
						<div className="btn-group">
							<button onClick={this.enableDevice.bind(this)} className="btn btn-xs btn-default">Habilitar</button>
							<button onClick={this.disableDevice.bind(this)} className="btn btn-xs btn-default">Desabilitar</button>
							{groupSelector}
						</div>
					</div>
					<hr />
					<MouseAreaSelector onSelecting={this.onMouseSelecting.bind(this)} />
					{_.map(devices, (device) => (
					<Device device={device}
							ref={'device-' + device.id}
							selected={selectedDevicesMap[device.id]}
							onClick={(selectOnly) => this.onDeviceClicked(device, selectOnly)}
							key={device.id} />))}
				</div>;
	}
}

export default connect(DeviceStore, GroupStore)(DeviceList);