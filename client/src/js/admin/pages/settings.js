import React, { Component } from 'react';
import _ from 'underscore';
import { connect } from '../myflux';
import SettingStore from '../stores/settings';
import ActivityStore from '../stores/activity';
import DatabaseStore from '../stores/database';
import Groups from './groups'
import ReportsPage from './reports'

class SettingsPage extends Component {
	resetDatabase(event) {
		if (event) event.preventDefault();
	}

	loadDatabase(event) {
		if (event) event.preventDefault();
		DatabaseStore.load();
	}

	saveDatabase(event) {
		if (event) event.preventDefault();
		DatabaseStore.save(function (err) {
			if (!err) ActivityStore.clear(true);
		});
	}

	saveSettings(event) {
		if (event) event.preventDefault();

		SettingStore.set(
			{
				numberOfDevices: this.refs.numberOfDevicesInput.value,
				defaultDeviceValue: this.refs.defaultDeviceValue.value
			});
	}

	render() {
		let settings = SettingStore.get();
		let currentDatabase = DatabaseStore.get();

		return (
			<div className="row">

				<fieldset  className="col-xs-offset-2 col-xs-8">
					<legend>Banco de Dados:</legend>

					<div className="col-xs-8 form-group">
						<input disabled className="form-control" value={currentDatabase || 'Nenhum arquivo carregado'} />
					</div>

					<div className="col-xs-4 text-right form-group">
						<a href="#" onClick={this.saveDatabase.bind(this)} target="_blank" className="btn btn-default">Novo</a>
						&nbsp;
							<a href="#" onClick={this.loadDatabase.bind(this)} target="_blank" className="btn btn-default">Abrir</a>
					</div>
				</fieldset>
				

				{/*Dispositivos config*/}
				<fieldset className="col-xs-offset-2 col-xs-8">
					<legend>Dispositivos:</legend>

					<div className="col-xs-4 form-group">
						<label>Nº Dispositivos</label>
						{settings.numberOfDevices == null ? null : <input className="form-control" defaultValue={settings.numberOfDevices} ref="numberOfDevicesInput" />}
					</div>

					<div className="col-xs-4 form-group">
						<label>Valor Padrão</label>
						{settings.defaultDeviceValue == null ? null : <input className="form-control" defaultValue={settings.defaultDeviceValue} ref="defaultDeviceValue" />}
					</div>

					<div className="col-xs-4  text-right form-group">
						<br/>
						<button onClick={this.saveSettings.bind(this)} className="btn btn-primary">Salvar</button>
					</div>
				</fieldset>
				<br/>
				<br/>
				<br/>
				{/*Groups*/}
				<fieldset className="col-xs-offset-2 col-xs-8 ">
					<legend>Grupos:</legend>
					<div>
						<Groups />
					</div>
				</fieldset>
				<br/>
				<br/>
				<br/>
				{/*Reportds*/}
				<fieldset className="col-xs-offset-2 col-xs-8 ">
					<legend>Relatorios:</legend>
					<div>
						<ReportsPage />
					</div>
				</fieldset>

			</div>

		);
	}
}

export default connect(SettingStore, DatabaseStore)(SettingsPage);