import { Observable } from 'rx';
import TinyEmitter from 'tiny-emitter';

const PAUSED		= 'PAUSED';
const PLAYING		= 'PLAYING';
const COMPLETED		= 'COMPLETED';
const NOT_STARTED	= 'NOT_STARTED';
const STATUS_CHANGE	= 'status-change';

export default class Playback extends TinyEmitter
{
	constructor(records)
	{
		super();
		this._records = records;
		this._currentIndex = 0;
		this._status = NOT_STARTED;
		this._interval = null;
		this._stream = Observable.fromEvent(this, 'value');
	}

	play()
	{
		if(this._status == PLAYING) return;
		if(this._status == COMPLETED) this._currentIndex = 0;

		this._status = PLAYING;
		this._interval = setInterval(this.onInterval.bind(this), 1000);
		this.emit(STATUS_CHANGE);
	}

	pause()
	{
		if(this._status != PLAYING) return;
		this._status = PAUSED;
		clearInterval(this._interval);
		this.emit(STATUS_CHANGE);
	}

	getStatus()
	{
		return this._status;
	}

	getStream()
	{
		return this._stream;
	}

	onInterval()
	{
		const index = this._currentIndex;

		if(index == this._records.length)
		{
			this._status = COMPLETED;
			this.emit(STATUS_CHANGE);
			clearInterval(this._interval);
		}
		else
		{
			this.emit('value', this._records[index]);
			this._currentIndex++;
		}
	}

	dispose()
	{
		this.off();
		clearInterval(this._interval);
	}
}