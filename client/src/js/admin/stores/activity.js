import _ from 'underscore';
import { Observable } from 'rx';
import api from '../api';
import socket from '../../common/socket-io';
import { createStore } from '../myflux';
import Playback from '../playback';

let firstFetch = false;
let running = false;
let records = [];

function fetch()
{
	api('Activity/running', null, null, (err, response) =>
	{
		if(err) return;

		running = response;
		this.emitChange();
	});

	api('Activity/values', null, null, (err, response) =>
	{
		if(err) return;

		_.each(response, function(item)
		{
			records.push(item);
		});

		firstFetch = true;

		this.emitChange();
	});
}

function onActivityStarted()
{
	running = true;
	this.emitChange();
}

function onActivityStopped()
{
	running = false;
	this.emitChange();
}

function onActivityCleared()
{
	records = [];
	this.emit('clear');
	this.emitChange();
}

function onNewValues(event)
{
	records.push(event);
	this.emit('new-value', event);
	this.emitChange();
}

export default createStore(
{
	initialize()
	{
		top.test = this;
		fetch.call(this);
		socket.on('activity-started', onActivityStarted.bind(this));
		socket.on('activity-stopped', onActivityStopped.bind(this));
		socket.on('activity-cleared', onActivityCleared.bind(this));
		socket.on('activity-new-values', onNewValues.bind(this));
	},
	start()
	{
		api('Activity/start', 'PUT');
	},
	stop()
	{
		api('Activity/stop', 'PUT');
	},
	breakpoint()
	{
		api('Devices/resetValue', 'PUT');
		api('Activity/breakpoint', 'PUT');
	},
	clear()
	{
		api('Activity/clear', 'PUT');
	},
	isRunning()
	{
		return running;
	},
	getStream()
	{
		if(!firstFetch) return null;

		let currentRecord$ = Observable.from(records).takeLast(19);
		let newRecord$ = Observable.fromEvent(this, 'new-value');
		return Observable.concat(currentRecord$, newRecord$);
	},
	onFirstFetch(callback)
	{
		let timeout;

		function check()
		{
			if(firstFetch) return callback();
			else timeout = setTimeout(check, 10);
		}

		check();
		return () => clearTimeout(timeout);
	},
	newPlayback()
	{
		if(!firstFetch) return null;
		return new Playback(records);
	}
});