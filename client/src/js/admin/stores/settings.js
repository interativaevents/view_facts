import api from '../api';
import { createStore } from '../myflux';

let settings = {};

export default createStore(
{
	initialize()
	{
		this.fetch();
	},
	fetch()
	{
		api('Settings/get', null, null, (err, response) =>
		{
			if(err) return;

			settings = response;
			this.emitChange();
		});
	},
	get()
	{
		return settings;
	},
	set(newSettings)
	{
		let data = { ...settings, ...newSettings };
		
		api('Settings/set', 'POST', { settings: data }, (err, response) =>
		{
			if(err) return;

			settings = response;
			this.emitChange();
		});
	}
});