import async from 'async';
import api from '../api';
import _ from 'underscore';
import socket from '../../common/socket-io';
import { createStore } from '../myflux';

let firstFetch = false;
let devices = {};
let deviceGroups = {};

function fetch()
{
	async.parallel(
	[
		(next) => api('Devices', null, null, (err, response) =>
		{
			if(!err) top.devices = devices = _.indexBy(response, 'id');
			return next(err);
		}),
		(next) => api('DeviceGroups', null, null, (err, response) =>
		{
			if(!err) top.deviceGroups = deviceGroups = _.indexBy(response, 'deviceId');
			return next(err);
		}),
	], () =>
	{
		firstFetch = true;
		this.emitChange();
	});
}

function onDeviceCreatedOrUpdated(device)
{
	devices[device.id] = device;
	this.emitChange();
}

function onDeviceDestroyed(id)
{
	delete devices[id];
	this.emitChange();
}

function onGroupsChanged(response)
{
	top.deviceGroups = deviceGroups = _.indexBy(response, 'deviceId');
	this.emitChange();
}

export default createStore(
{
	initialize()
	{
		fetch.call(this);
		socket.on('device-created', onDeviceCreatedOrUpdated.bind(this));
		socket.on('device-updated', onDeviceCreatedOrUpdated.bind(this));
		socket.on('device-destroyed', onDeviceDestroyed.bind(this));
		socket.on('device-groups-changed', onGroupsChanged.bind(this));
	},
	get()
	{
		return _.chain(devices)
				.map(function(device)
				{
					let { groupId } = deviceGroups[device.id] || {};
					return { ...device, groupId };
				})
				.indexBy('id')
				.value();
	},
	enableDevice(ids)
	{
		api('Devices/enableDevices', 'PUT', { ids: _.flatten([ids]) });
	},
	disableDevice(ids)
	{
		api('Devices/disableDevices', 'PUT', { ids: _.flatten([ids]) });
	},
	addToGroup(groupId, ids)
	{
		api('Devices/addToGroup', 'PUT', { groupId, ids: _.flatten([ids]) });
	},
	removeDevices(ids)
	{
		api('Devices/removeFromGroup', 'PUT', { ids: _.flatten([ids]) });
	},
	resetValue()
	{
		api('Devices/resetValue', 'PUT');
	},
	onFirstFetch(callback)
	{
		let timeout;

		function check()
		{
			if(firstFetch) return callback();
			else timeout = setTimeout(check, 10);
		}

		check();
		return () => clearTimeout(timeout);
	}
});