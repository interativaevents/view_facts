import api from '../api';
import _ from 'underscore';
import { createStore } from '../myflux';
import socket from '../../common/socket-io';

const COLORS = ['#e41a1c', '#377eb8', '#4daf4a', '#984ea3', '#ff7f00', '#444444', '#a65628', '#f781bf', '#ffff33'];
const getColor = (i) => COLORS[i % COLORS.length];

let firstFetch = false;
let groups = {};

function fetch()
{
	api('Groups', null, null, (err, response) =>
	{
		if(err) return;

		top.groups = groups = _.chain(response).union([/*{ name: 'Nenhum Grupo', id: -1 }*/]).map(function(group, i)
		{
			group.color = getColor(i);
			return group;
		}).indexBy('id').value();

		this.emitChange();

		firstFetch = true;
	});
}

export default createStore(
{
	initialize()
	{
		const bindedFetch = fetch.bind(this);

		socket.on('groups-changed', bindedFetch);
		bindedFetch();
	},
	get()
	{
		return _.toArray(groups);
	},
	getById(id)
	{
		return groups[id];
	},
	save(group)
	{
		api('Groups', group.id ? 'PUT' : 'POST', group, (err, response) =>
		{
			// if(err) return;
			// response.color = getColor();
			// groups[response.id] = response;
			// this.emitChange();
		});
	},
	destroy(id)
	{
		api('Groups/' + id, 'DELETE', null, (err, response) =>
		{
			// if(err) return;
			// delete groups[id];
			// this.emitChange();
		});
	},
	onFirstFetch(callback)
	{
		let timeout;
		
		function check()
		{
			if(firstFetch) return callback();
			else timeout = setTimeout(check, 10);
		}
		
		check();
		return () => clearTimeout(timeout);
	}
});