import api from '../api';
import { createStore } from '../myflux';
import dialog from '../dialog';

let currentFile;

export default createStore(
{
	initialize()
	{
		this.fetch();
	},
	fetch()
	{
		const that = this;

		api('Database/currentFile', null, null, (err, response) =>
		{
			currentFile = response;

			if(currentFile)
			{
				this.emitChange();
			}
			else
			{
				dialog.custom('Você deseja abrir um banco de dados existente, ou criar um novo?',
				{
					'load': { text: 'Abrir' },
					'new': { text: 'Novo' }
				}, function(err, result)
				{
					if(result == 'new') that.save();
					else if(result == 'load') that.load();
				});
			}
		});
	},
	get()
	{
		return currentFile;
	},
	save(callback = function(){})
	{
		api('Database/save', null, null, (err, response) =>
		{
			if(err)
			{
				dialog.alert(err);
			}
			else if(response)
			{
				this.fetch();
				dialog.alert('Banco de dados salvo com sucesso');
			}

			return callback(err);
		});
	},
	load()
	{
		api('Database/load', null, null, (err, response) =>
		{
			if(err)
			{
				return dialog.alert(err);
			}
			else if(response)
			{
				return dialog.alert('Banco de dados carregado com sucesso', () => window.location.reload());
			}
		});
	}
});