import _ from 'underscore';
import async from 'async';
import moment from 'moment';
import React, { Component } from 'react';
import { render, findDOMNode } from 'react-dom';
import ActivityStore from '../stores/activity';
import GroupStore from '../stores/group';
import DeviceStore from '../stores/device';
import { connect } from '../myflux';

const MAX_ITEMS = 15;
let currentTime = '';

function callOrReturn(val)
{
	if(typeof val == 'function') return val();
	return val;
}

function resizeChart(controller, width, height)
{
	var chart = controller.chart;
	var canvas = chart.canvas;

	controller.clear();

	chart.width = callOrReturn(width);
	chart.height = callOrReturn(height);

	canvas.style.width = chart.width + 'px';
	canvas.setAttribute('width', chart.width);

	canvas.setAttribute('height', chart.height);
	canvas.style.height = chart.height + 'px';

	controller.update();
}

function parseSecond(totalSec)
{
	const minutes = parseInt(totalSec / 60);
	const seconds = totalSec % 60;

	return `${minutes < 10 ? '0' + minutes : minutes}:${seconds  < 10 ? '0' + seconds : seconds}`;
}

function drawDatasetPointsLabels()
{
	const seconds = {};
	const chartController = this;
	const { ctx } = chartController.chart;
	const { data, scales } = chartController;

	ctx.fillStyle = '#000';
	ctx.textAlign = "center";

	// ctx.font = '1.6rem "Gotham Book",sans-serif';
	// ctx.fillText(currentTime, ctx.canvas.offsetWidth / 2, 14);

	ctx.font = '1rem "Gotham Book",sans-serif';
	_.each(data.datasets, function(dataset, i)
	{
		_.each(dataset.data, function(value, j)
		{
			const points = seconds[j] = seconds[j] || {};
			const { x, y } = _.first(_.toArray(dataset._meta)).data[j]._model;

			if(!points[value])
			{
				points[value] = true;
				ctx.fillText(value, x, y - 20);
			}
		});
	});

	const breakpoints	= _.map(data.breakpoints, parseSecond);
	const xAxis			= scales['x-axis-0'];
	const yAxis			= scales['y-axis-0'];
	const xAxisWidth	= xAxis.width - xAxis.paddingRight;
	const yAxisHeight	= yAxis.height;
	const ticks			= xAxis.ticks;
	const tickCount		= ticks.length;
	const offsetLeft	= xAxis.left;
	const y				= yAxis.top;
	const height		= yAxis.height + y;

	_.each(breakpoints, function(second)
	{
		const index = ticks.indexOf(second);

		if(index > -1)
		{
			const x = offsetLeft + (index * xAxisWidth / (tickCount - 1));

			ctx.setLineDash([2, 2]);
			ctx.strokeStyle = '#F00';
			ctx.beginPath();
			ctx.moveTo(x, y);
			ctx.lineTo(x, height);
			ctx.stroke();
			ctx.setLineDash([]);

			ctx.font = '1.2rem "Gotham Book",sans-serif';
			ctx.fillStyle = '#F00';
			ctx.textAlign = "center";
			ctx.fillText(second, x + 20, y + 15);
		}
	});
}

const ActivityDetails = connect(DeviceStore)(function(props)
{
	const { groupLegends, hideDeviceCount } = props;
	const devices = DeviceStore.get();
	const connectedDevices = _.where(devices, { connected: true }).length;
	const totalDevices = _.size(devices);

	return	<div className="activity-chart-details clearfix">
				<div className="activity-chart-details-legend">{groupLegends}</div>
				{ hideDeviceCount ? null : <div className="activity-chart-details-info">{`Dispositivos: ${connectedDevices}/${totalDevices}`}</div> }
			</div>;
});

export default class ActivityChart extends Component
{
	constructor(props)
	{
		super(props);
		this.disposables = [];
		currentTime = '';
	}

	componentDidMount()
	{
		this.canvasNode = findDOMNode(this.refs.canvas);
		this.detailsNode = findDOMNode(this.refs.details);

		async.parallel(
		[
			(next) => this.disposables.push(GroupStore.onFirstFetch(next)),
			(next) => this.disposables.push(DeviceStore.onFirstFetch(next))
		], this.bootstrap.bind(this));

		this.disposables.push(() => clearTimeout(this.rerenderGraphTimeout));
	}

	bootstrap()
	{
		this.createChart();
		
		let finalStream = this.props.stream.map((record) =>
		{
			let devices = DeviceStore.get();
			let valuesByGroup = {};
			let result =
			{
				second: record.second,
				breakpoint: record.breakpoint,
			};

			record.values.forEach(function(deviceRecord)
			{
				let { deviceId, value } = deviceRecord, { groupId } = devices[deviceId] || {};


				if(!groupId) groupId = -1;
				if(!valuesByGroup[groupId]) valuesByGroup[groupId] = { sum: 0, devices: 0 };

				valuesByGroup[groupId].sum += value;
				valuesByGroup[groupId].devices += 1;
			});

			_.each(valuesByGroup, function(data, groupId)
			{
				result['group_' + groupId] = data.devices ? data.sum / data.devices : 0;
			});

			return result;
		});

		let subscription = finalStream.subscribe(this.onValue.bind(this));
		this.disposables.push(() => subscription.dispose());
	}

	clear()
	{
		this.chart.data.labels = _.range(MAX_ITEMS).map(parseSecond);
		this.chart.data.datasets = [];
		this.chart.data.breakpoints = [];
		this.chart.update();

		render(<ActivityDetails hideDeviceCount={this.props.hideDeviceCount} />, this.detailsNode);
	}

	onValue(record)
	{
		const groups = GroupStore.get();
		const { chart, detailsNode } = this;
		const { labels, datasets } = chart.data;
		const groupLegends = [];

		if(groups.length == 0)
		{
			groups.push({ id: -1, name: 'Nenhum Grupo', color: '#0000FF' });
		}

		if(record.second >= MAX_ITEMS)
		{
			labels.push(parseSecond(record.second));
		}

		currentTime = parseSecond(record.second);

		findDOMNode(this.refs.clock).innerHTML = currentTime;

		if(MAX_ITEMS) chart.data.labels = _.last(labels, MAX_ITEMS);

		if(record.breakpoint)
		{
			chart.data.breakpoints = chart.data.breakpoints || [];
			chart.data.breakpoints.push(record.second);
		}

		groups.forEach(function({ id, color, name })
		{
			const value = record[`group_${id}`];
			let dataset = _.findWhere(datasets, { groupId: id });

			if(!dataset)
			{
				datasets.push(dataset =
				{
					groupId: id,
					data: [],
					lineTension: 0,
					fill: false,
					backgroundColor: color,
					borderColor: color,
					label: name
				});
			}

			groupLegends.push(
				<div className="activity-chart-details-legend-item" key={id} style={{color: color}}>
					<span className="activity-chart-details-legend-item-color" />
					<span className="activity-chart-details-legend-item-text">{`${name}: ${(value == null ? 'Sem dados' : Math.round(value,-2))}`}</span>
				</div>
			);

			dataset.data.push(Math.round(value,-2));
			if(MAX_ITEMS) dataset.data = _.last(dataset.data, MAX_ITEMS);
		});

		render(<ActivityDetails hideDeviceCount={this.props.hideDeviceCount} groupLegends={groupLegends} />, detailsNode);
		
		this.resizeAndUpdate();
		// chart.update();
	}

	componentWillUnmount()
	{
		this.disposables.forEach(func => func());
	}

	createChart()
	{
		const canvas = this.canvasNode;
		const chart = this.chart = new Chart(canvas,
		{
			type: 'line',
			data: { labels: _.range(MAX_ITEMS).map(parseSecond), datasets: [] },
			options:
			{
				responsive: false,
				maintainAspectRatio: false,
				title:
				{
					display: true
				},
				tooltips:
				{
					enabled: false
				},
				legend:
				{
					display: false
				},
				animation:
				{
					duration: 0,
					onProgress: drawDatasetPointsLabels,
					onComplete: drawDatasetPointsLabels  
				},
				scales:
				{
					yAxes:
					[{
						type: 'linear',
						ticks:
						{
							beginAtZero: true,
							min: 0,
							max: 100,
							stepSize: 10
						}
					}]
				}
			}
		});

		const calcChartWidth	= this.props.width || function() { return canvas.parentNode.offsetWidth; };
		const calcChartHeight	= this.props.height || 350;
		const resizeAndUpdate	= this.resizeAndUpdate = () => resizeChart(chart, calcChartWidth, calcChartHeight);

		window.addEventListener('resize', resizeAndUpdate);
		resizeAndUpdate();

		this.disposables.push(() => window.removeEventListener('resize', resizeAndUpdate));
		this.disposables.push(() => this.chart.destroy());
	}

	shouldComponentUpdate()
	{
		return false;
	}


	render()
	{
		return	<div>
					<div ref="clock" className="activity-chart-clock" />
					<canvas ref="canvas" className="activity-chart-canvas" />
					<div ref="details" />
				</div>;
	}
}