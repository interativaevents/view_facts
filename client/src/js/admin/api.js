var xhr = require("xhr")

export default function api(path, method = 'GET', data, callback = function(){})
{
	return xhr(
	{
		method,
	    body: JSON.stringify(data),
	    uri: '/api/' + path,
	    headers: { 'Content-Type': 'application/json' }
	}, function(err, res, body)
	{
		if(err)
		{
			return callback(err);
		}
		else if(res.statusCode != 200)
		{
			return callback(res);
		}
		else try
		{
			return callback(null, JSON.parse(body));
		}
		catch(err)
		{
			return callback(err);
		}
	});
};