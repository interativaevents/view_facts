import _ from 'underscore';
import React from 'react';
import { render } from 'react-dom';

const body = document.body;
let counter = 0;

function modal(text, buttons, callback = function(){})
{
	const modal = document.createElement('div');
	const backdrop = document.createElement('div');

	counter++;
	modal.style.display = 'block';
	modal.className = 'modal fade in';
	backdrop.className = 'modal-backdrop fade in';
	body.classList.add('modal-open');

	body.appendChild(modal);
	body.appendChild(backdrop);

	function close()
	{
		modal.parentNode.removeChild(modal);
		backdrop.parentNode.removeChild(backdrop);
		if(--counter == 0) body.classList.remove('modal-open');
	}

	function onClick(key)
	{
		callback(null, key);
		close();
	}

	render((
		<div className="modal-dialog">
			<div className="modal-content">
				<div className="modal-body">{text}</div>
				<div className="modal-footer">
				{_.map(buttons, function({type, text}, key)
				{
					return <button
							key={key}
							className={`btn btn-sm btn-${type || 'default'}`}
							onClick={onClick.bind(null, key)}>{text}</button>;
				})}
				</div>
			</div>
		</div>
	), modal);
}

function alert(text, callback)
{
	return modal(text, { ok: { text: 'Ok' } }, callback);
}

function confirm(text, callback)
{
	return modal(text, { no: { text: 'Não' }, yes: { text: 'Sim', type: 'primary' } }, callback);
}

const dialog = { custom: modal, alert, confirm };

export default dialog;