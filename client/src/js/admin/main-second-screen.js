import _ from 'underscore';
import React, { Component } from 'react';
import { render } from 'react-dom';
import socket from '../common/socket-io';

import { connect } from './myflux';
import DeviceStore from './stores/device';
import GroupStore from './stores/group';
import ActivityStore from './stores/activity';
import ActivityChart from './components/activity-chart';

function calcChartWidth()
{
	var canvas = document.querySelector('canvas');

	if(canvas)
	{
		return canvas.parentNode.offsetWidth;
	}
}

function calcChartHeight()
{
	var canvas = document.querySelector('canvas');
	var details = document.querySelector('.activity-chart-details');

	if(canvas && details)
	{
		return window.innerHeight - details.offsetHeight - 100;
	}
}

class ActivityPage extends Component
{
	constructor(props)
	{
		super(props);
		this.disposables = [];
		this.state = { activityStream: null };
	}

	componentDidMount()
	{
		this.disposables.push(ActivityStore.onFirstFetch(this.onActivityFetched.bind(this)));
		this.disposables.push(ActivityStore.on('clear', this.onActivityCleared.bind(this)));
	}

	componentWillUnmount()
	{
		this.disposables.forEach(func => func());
	}

	onActivityFetched()
	{
		const activityStream = ActivityStore.getStream();
		this.setState({ activityStream });
	}

	onActivityCleared()
	{
		if(this.refs && this.refs.activityChart)
		{
			this.refs.activityChart.clear();
		}
	}

	render()
	{
		const { activityStream } = this.state;
		const running = ActivityStore.isRunning();
		
		if(activityStream)
		{		
			return <ActivityChart width={calcChartWidth} height={calcChartHeight} ref="activityChart" stream={activityStream} />;
		}

		return null;
	}
}

const App = connect(ActivityStore)(ActivityPage);

function reload()
{
	var node = document.getElementById('app');
	render(<div />, node);
	setTimeout(() => render(<App />, node), 10);
}

socket.on('database-loaded', reload);
GroupStore.listenTo(reload);
reload();