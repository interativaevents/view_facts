import React, { Component } from 'react';
import TinyEmitter from 'tiny-emitter';

class StoreEmitter extends TinyEmitter
{
	on(e, h)
	{
		super.on(e, h);
		return () => this.off(e, h);
	}

	emitChange()
	{
		this.emit('change');
	}

	listenTo(callback)
	{
		this.on('change', callback);
		return () => this.off('change', callback);
	}
}

export function createStore(methods)
{
	const store = {...methods};
	const emitter = new StoreEmitter();

	store.once = emitter.once.bind(emitter);
	store.on = emitter.on.bind(emitter);
	store.off = emitter.off.bind(emitter);
	store.emit = emitter.emit.bind(emitter);
	store.emitChange = emitter.emitChange.bind(emitter);
	store.listenTo = emitter.listenTo.bind(emitter);

	if(store.initialize)
	{
		store.initialize();
		delete store.initialize;
	}

	return store;
};

export function connect(...stores)
{
	return function(Element)
	{
		return class ConnectedElement extends Component
		{
			constructor(props, context)
			{
				super(props, context);
				this.__listeners = [];

				const forceUpdate = () => this.forceUpdate();
				stores.forEach((store) => this.__listeners.push(store.listenTo(forceUpdate)));
			}

			componentWillUnmount()
			{
				this.__listeners.forEach(off => off());
			}

			render()
			{
				const { props } = this;
				return <Element {...props} />;
			}
		};
	}
};