import serverAddress from './server-address';
var socket = io(serverAddress());

export default socket;