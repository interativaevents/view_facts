mkdir build
rm -rf build/viewfacts-win32-x64
rm -rf temp
cd client
npm install
gulp build -p
cd ..
cp -R electron temp
cp -R local-server temp/
cd temp
npm install
cd local-server
npm install
cd ..
./node_modules/.bin/electron-packager . viewfacts --overwrite --platform=win32 --arch=x64 --asar --prune=true
cd ..
cp -R temp/viewfacts-win32-x64 build/viewfacts-win32-