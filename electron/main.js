const { app, BrowserWindow, dialog } = require('electron');

let server;
try { server = require('./local-server'); }
catch(err) { server = require('../local-server'); }

let mainWindow;

let serverStarted = false;

app.on('ready', function()
{
	mainWindow = new BrowserWindow(
	{
		'width': 1110,
		'height': 600,
		'minWidth': 1110
	});

	mainWindow.loadURL(`file://${__dirname}/html/index.html`);
	mainWindow.on('closed', () => app.quit());
});


app.on('start-server', function(host, port)
{
	if(serverStarted) return;

	serverStarted = true;

	server.start(port, function(err)
	{
		if(err)
		{
			if(err.code == 'EADDRINUSE')
			{
				mainWindow.webContents.executeJavaScript('alert("A porta informada já está em uso.");');
			}

			serverStarted = false;
			return;
		}

		server.on('show-open-dialog', function(options, callback)
		{
			dialog.showOpenDialog(mainWindow, options, callback);
		});

		server.on('show-save-dialog', function(options, callback)
		{
			dialog.showSaveDialog(mainWindow, options, callback);
		});
		
		return mainWindow.loadURL(`http://${host}:${port}/admin`);
	});
});
